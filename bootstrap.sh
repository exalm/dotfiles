FILES="config/fish/functions/fish_prompt.fish config/fish/config.fish config/fish/fish_variables config/mc/ini config/mc/filehighlight.ini local/share/mc/skins/transparent.ini local/share/mc/skins/transparent-blue.ini local/share/mc/skins/transparent-gray.ini local/share/mc/skins/solarized-dark.ini XCompose gitconfig"
DIRS=""
BACKUP=""

ORIG=$(cd `dirname $0` && pwd)
DEST=$HOME

RESTORE=""

if [ "$1" != "" ]; then
    DEST="$1"
fi

for F in $BACKUP; do
    if [ -f "$DEST/.$F" ]; then
        mkdir -p "$ORIG/backup/$(dirname $F)"
        cp "$DEST/.$F" "$ORIG/backup/$F"
        RESTORE="$RESTORE $F"
    fi
done

for F in $FILES; do
    mkdir -p "$DEST/.$(dirname $F)"
    cp "$ORIG/$F" "$DEST/.$F"
done

for D in $DIRS; do
    rm -rf "$DEST/.$D"
    cp -r "$ORIG/$D" "$DEST/.$D"
done

for F in $RESTORE; do
    mkdir -p "$DEST/.$(dirname $F)"
    cp "$ORIG/backup/$F" "$DEST/.$F"
done
rm -rf "$ORIG/backup"

sudo chsh $USER -s /bin/fish
