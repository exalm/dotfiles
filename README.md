# dotfiles
Simple fish, mc, git, XCompose configs

# installation

```
git clone --recursive https://gitlab.com/alice-m/dotfiles.git
cd dotfiles
./bootstrap.sh
```

An additional argument can be provided for `bootstrap.sh` to make it install into a different directory.
