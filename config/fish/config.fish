set -gx PATH \
/bin:\
/sbin:\
/usr/local/bin:\
/usr/local/sbin:\
/usr/bin:\
/usr/games:\
/usr/sbin:\
/usr/share/Modules/bin:\
/usr/lib64/ccache:\
/var/lib/flatpak/exports/bin:\
/home/alice/bin:\
/home/alice/.bin:\
/home/alice/.local/bin:\
/home/alice/.local/share/flatpak/exports/bin:\
/home/alice/.cargo/bin

set -gx EDITOR mcedit
